import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
    UserModel({
        this.online,
        this.alias,
        this.email,
        this.id,
    });

    bool online;
    String alias;
    String email;
    String id;

    factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        online: json["online"],
        alias: json["alias"],
        email: json["email"],
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "online": online,
        "alias": alias,
        "email": email,
        "id": id,
    };
}
