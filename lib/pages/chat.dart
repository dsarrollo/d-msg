import 'dart:io';

import 'package:chat_app/widgets/msg-chat.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({Key key}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> with TickerProviderStateMixin {
  final _inputCtrl = TextEditingController();
  final _focusNode = FocusNode();
  List<MsgChat> _listMessages = [];
  bool writing = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        title: Column(
          children: [
            CircleAvatar(
              child: Text(
                'Us',
                style: TextStyle(
                  fontSize: 12.0,
                ),
              ),
              backgroundColor: Colors.blue[200],
              maxRadius: 16,
            ),
            Text(
              'Usuario tal',
              style: TextStyle(
                fontSize: 12.0,
                color: Colors.blue[200],
              ),
            ),
          ],
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Flexible(
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                itemBuilder: (_, i) => _listMessages[i],
                itemCount: _listMessages.length,
                reverse: true,
              ),
            ),
            Divider(height: 1),
            Container(
              color: Colors.white,
              child: _inputchat(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _inputchat() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: [
          Flexible(
            child: TextField(
              controller: _inputCtrl,
              onSubmitted: _handleSubmit,
              onChanged: (value) {
                setState(() {
                  writing = false;
                  if (value.trim().length > 0) {
                    writing = true;
                  }
                });
              },
              decoration: InputDecoration.collapsed(hintText: 'Enviar mensaje'),
              focusNode: _focusNode,
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 5.0),
            child: Platform.isIOS
                ? CupertinoButton(
                    child: Text('Enviar'),
                    onPressed: writing
                        ? () => _handleSubmit(_inputCtrl.text.trim())
                        : null,
                  )
                : IconTheme(
                    data: IconThemeData(color: Colors.blue[300]),
                    child: IconButton(
                      highlightColor: Colors.transparent,
                      splashColor: Colors.transparent,
                      onPressed: writing
                          ? () => _handleSubmit(_inputCtrl.text.trim())
                          : null,
                      icon: Icon(Icons.send),
                    ),
                  ),
          ),
        ],
      ),
    );
  }

  _handleSubmit(String msg) {
    if (msg.length == 0) return;
    final newMessage = MsgChat(
      msg: msg,
      typeUser: '123',
      animation: AnimationController(
          vsync: this, duration: Duration(milliseconds: 400)),
    );
    _listMessages.insert(0, newMessage);
    newMessage.animation.forward();
    setState(() {
      writing = false;
    });
    _inputCtrl.clear();
    _focusNode.requestFocus();
  }

  @override
  void dispose() {
    for (MsgChat item in _listMessages) {
      item.animation.dispose();
    }
    super.dispose();
  }
}
