import 'package:chat_app/widgets/alert.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:chat_app/services/auth.dart';
import 'package:chat_app/widgets/btn-primary.dart';
import 'package:chat_app/widgets/footer-auth.dart';
import 'package:chat_app/widgets/logo.dart';
import 'package:chat_app/widgets/input.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.93,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Logo(titleText: 'D-MSG Register'),
                _Form(),
                LabelsFooter(
                  textOne: '¿Ya tienes cuenta?',
                  textTwo: 'Inicia sessión ahora!',
                  navigator: 'login',
                ),
                Text(
                  'Terminos y condiciones de uso',
                  style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.w200,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Form extends StatefulWidget {
  @override
  __FormState createState() => __FormState();
}

class __FormState extends State<_Form> {
  final aliasCtrl = TextEditingController();
  final emailCtrl = TextEditingController();
  final passCtrl = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: false);
    return Container(
      margin: EdgeInsets.only(top: 40.0),
      padding: EdgeInsets.symmetric(horizontal: 50.0),
      child: Column(
        children: [
          Input(
            icon: Icons.person_outline,
            placeholder: 'Alias',
            textController: aliasCtrl,
          ),
          Input(
            icon: Icons.mail_outline,
            placeholder: 'Correo',
            textController: emailCtrl,
          ),
          Input(
              icon: Icons.lock_outline,
              placeholder: 'Contraseña',
              textController: passCtrl,
              isPassowrd: true),
          BtnPrimary(
            text: 'Registrarse',
            onPressed: !authService.loading
                ? () async {
                    FocusScope.of(context).unfocus();
                    final status = await authService.register(
                        aliasCtrl.text.trim(),
                        emailCtrl.text.trim(),
                        passCtrl.text.trim());
                    if (status) {
                      Navigator.pushReplacementNamed(context, 'users');
                    } else {
                      // alerta de error
                      showAlert(context, 'Registro fallido', 'email ya existe');
                    }
                  }
                : null,
          ),
        ],
      ),
    );
  }
}
