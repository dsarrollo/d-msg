import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:chat_app/models/user.dart';

class UsersPage extends StatefulWidget {
  const UsersPage({Key key}) : super(key: key);

  @override
  _UsersPageState createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  final users = [
    UserModel(id: '1', alias: 'Eduardo', email: 'test1@test.com', online: true),
    UserModel(id: '2', alias: 'Alejandro', email: 'test2@test.com', online: true),
    UserModel(id: '3', alias: 'Maria', email: 'test3@test.com', online: false),
    UserModel(id: '4', alias: 'Pedro', email: 'test4@test.com', online: true),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Lista de usuarios',
          style: TextStyle(color: Color(0xff77b9d2)),
        ),
        elevation: 1,
        backgroundColor: Colors.white,
        leading: IconButton(
          color: Colors.grey[400],
          icon: Icon(Icons.exit_to_app),
          onPressed: () {},
        ),
        actions: [
          Container(
            padding: EdgeInsets.only(right: 10.0),
            child: Icon(
              Icons.check_circle_outline,
              color: Colors.green[300],
            ),
          ),
        ],
      ),
      body: SmartRefresher(
        controller: _refreshController,
        enablePullDown: true,
        onRefresh: _loadUsers,
        header: WaterDropHeader(
          complete: Icon(Icons.check, color: Color(0xff77b9d2)),
          waterDropColor: Color(0xff77b9d2),
        ),
        child: _usersListView(),
      ),
    );
  }

  ListView _usersListView() {
    return ListView.separated(
      physics: BouncingScrollPhysics(),
      itemBuilder: (_, i) => _usersListTile(users[i]),
      separatorBuilder: (_, i) => Divider(),
      itemCount: users.length,
    );
  }

  ListTile _usersListTile(UserModel user) {
    return ListTile(
      title: Text(user.alias),
      subtitle: Text(
        user.email,
        style: TextStyle(fontSize: 12.0),
      ),
      leading: CircleAvatar(
        backgroundColor: Color(0xff77b9d2),
        child: Text(
          user.alias.substring(0, 2),
          style: TextStyle(color: Colors.white),
        ),
      ),
      trailing: Icon(
        Icons.circle,
        color: user.online ? Colors.green[200] : Colors.grey[400],
        size: 12,
      ),
    );
  }

  void _loadUsers() async {
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }
}
