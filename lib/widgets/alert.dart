import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

showAlert(BuildContext context, String title, String content) {
  Platform.isIOS
      ? showCupertinoDialog(
          context: context,
          builder: (_) => CupertinoAlertDialog(
            title: Text(title),
            content: Text(content),
            actions: [
              CupertinoDialogAction(
                isDefaultAction: true,
                child: Text('Ok'),
                onPressed: () => Navigator.pop(context),
              ),
            ],
          ),
        )
      : showDialog(
          context: context,
          builder: (_) => AlertDialog(
            title: Text(title),
            content: Text(content),
            actions: [
              MaterialButton(
                child: Text('Ok'),
                elevation: 5,
                textColor: Color(0xff77b9d2),
                onPressed: () => Navigator.pop(context),
              ),
            ],
          ),
        );
}
