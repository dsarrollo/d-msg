import 'package:flutter/material.dart';

class MsgChat extends StatelessWidget {
  final String msg;
  final String typeUser;
  final AnimationController animation;

  const MsgChat({
    Key key,
    @required this.msg,
    @required this.typeUser,
    @required this.animation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: this.animation,
      child: SizeTransition(
        sizeFactor: CurvedAnimation(parent: animation, curve: Curves.easeOut),
        child: Container(
          child: this.typeUser == '123' ? _myMessage() : _otherMessage(),
        ),
      ),
    );
  }

  Widget _myMessage() {
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        padding: EdgeInsets.all(5.0),
        margin: EdgeInsets.only(bottom: 4.0, right: 5.0, left: 20.0),
        decoration: BoxDecoration(
          color: Colors.grey[700],
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Text(
          this.msg,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget _otherMessage() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        padding: EdgeInsets.all(5.0),
        margin: EdgeInsets.only(bottom: 4.0, left: 5.0, right: 20.0),
        decoration: BoxDecoration(
          color: Color(0xff77b9d2),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Text(this.msg, style: TextStyle(color: Colors.white)),
      ),
    );
  }
}
