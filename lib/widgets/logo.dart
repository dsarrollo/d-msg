import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  final String titleText;

  const Logo({Key key, @required this.titleText}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.only(top: 40.0),
        child: Column(children: [
          Image(
            image: AssetImage('assets/chatIcon.png'),
            width: 120.0,
          ),
          Text(
            this.titleText,
            style: TextStyle(fontSize: 25, color: Colors.grey[800]),
          ),
        ]),
      ),
    );
  }
}