import 'package:flutter/material.dart';

class BtnPrimary extends StatelessWidget {
  final String text;
  final Function onPressed;
  const BtnPrimary({Key key, @required this.text, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 2,
      highlightElevation: 5,
      color: Color(0xff77b9d2),
      shape: StadiumBorder(),
      child: Container(
          width: double.infinity,
          child: Center(
              child: Text(
            this.text,
            style: TextStyle(
              color: Colors.white,
              fontSize: 15.0,
            ),
          ))),
      onPressed: this.onPressed,
    );
  }
}
