import 'package:flutter/material.dart';

class LabelsFooter extends StatelessWidget {
  final String textOne;
  final String textTwo;
  final String navigator;

  const LabelsFooter({
    Key key,
    @required this.textOne,
    @required this.textTwo,
    @required this.navigator,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            this.textOne,
            style: TextStyle(
              color: Colors.black54,
              fontSize: 15,
              fontWeight: FontWeight.w400,
            ),
          ),
          SizedBox(height: 10.0),
          GestureDetector(
            child: Text(
              this.textTwo,
              style: TextStyle(
                color: Color(0xff77b9d2),
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              Navigator.pushReplacementNamed(context, this.navigator);
            },
          )
        ],
      ),
    );
  }
}
