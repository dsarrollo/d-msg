import 'dart:convert';
import 'package:chat_app/models/login.dart';
import 'package:chat_app/models/user.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:chat_app/environments/environment.dart';

class AuthService with ChangeNotifier {
  UserModel user;
  bool _loading = false;
  // Create storage
  final _storage = new FlutterSecureStorage();

  bool get loading => this._loading;
  set loading(bool value) {
    this._loading = value;
    notifyListeners();
  }

  // getters al token staticas
  static Future<String> getToken() async {
    final _storage = new FlutterSecureStorage();
    final token = await _storage.read(key: 'token');
    return token;
  }

  Future<bool> login(String email, String password) async {
    this.loading = true;
    final dataSend = {
      'email': email,
      'password': password,
    };
    final resp = await http.post('${Environment.apiUrl}/login',
        body: jsonEncode(dataSend),
        headers: {
          'Content-type': 'application/json',
        });
    print(resp.body);
    print('==========================');
    this.loading = false;
    if (resp.statusCode == 200) {
      final loginSuccess = loginModelFromJson(resp.body);
      this.user = loginSuccess.user;
      await this._saveToken(loginSuccess.token);
      return true;
    }
    return false;
  }

  Future<bool> register(String alias, String email, String password) async {
    this.loading = true;
    final dataSend = {
      'alias': alias,
      'email': email,
      'password': password,
    };
    final resp = await http.post('${Environment.apiUrl}/register',
        body: jsonEncode(dataSend),
        headers: {
          'Content-type': 'application/json',
        });
    this.loading = false;
    if (resp.statusCode == 200) {
      final loginSuccess = loginModelFromJson(resp.body);
      this.user = loginSuccess.user;
      await this._saveToken(loginSuccess.token);
      return true;
    }
    return false;
  }

  Future _saveToken(String token) async {
    return await _storage.write(key: 'token', value: token);
  }

  Future logout() async {
    await _storage.delete(key: 'token');
  }
}
